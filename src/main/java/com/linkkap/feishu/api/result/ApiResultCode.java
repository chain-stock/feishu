package com.linkkap.feishu.api.result;

/**
 * @program: web-base
 * @author: WANGYI
 * @create: 2020-09-18 10:37
 * @description: 响应的代码
 **/
public enum ApiResultCode {

    /**
     * 未知错误，一般是RuntimeException导致
     */
    UNKNOWN(-1,"系统业务错误"),
    /**
     * 成功
     */
    SUCCESS(200,"成功"),

    /**
     * 系统异常，一般是不清楚的方法产生的Exception
     */
    SYSTEM_ERROR(500,"系统异常"),

    /**
     * 数据不存在，入参错误或者是数据错误
     */
    DATA_ERROR(400,"请求的相关数据错误"),

    /**
     * 参数验证出错
     */
    PARAMETER_VERIFY_ERROR(401,"参数验证出错,传入的参数不符合API的要求"),

    /**
     * 幂等错误
     */
    IDEMPOTENT_ERROR(402, "幂等错误"),

    /**
     * 找不到版本对应的api
     */
    NO_SUCH_VERSION_API(404,"找不到版本对应的api"),

    /**
     * session_key错误
     */
    SESSION_KEY_ERROR(20001,"session_key错误"),

    /**
     * 无效的用户
     */
    USER_INVALID(20002,"无效的用户"),

    /**
     * 验证码错误
     */
    VERIFY_CODE_ERROR(20004,"验证码错误"),

    /**
     * 帐号密码错误
     */
    USER_LOGIN_ERROR(20005,"帐号密码错误"),

    /**
     * 无此权限
     */
    PERMISSION_DENIED(20006,"无此权限");


    /**
     * 错误码
     */
    private int code;


    /**
     * 通用的错误信息
     */
    private String message;

    ApiResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}