package com.linkkap.feishu.api.result;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @program: web-base
 * @author: WANGYI
 * @create: 2020-09-18 10:32
 * @description: 接口层返回的结果
 **/
@Data
@ApiModel(value = "接口响应结果", description = "返回的结果")
@Accessors(chain = true)
public class ApiResult<T> {

    private static final long serialVersionUID = 1L;

    /**
     *  成功的默认返回响应
     */
    public final static ApiResult<Object> SUCCESS ;

    static {
        SUCCESS = getResult(ApiResultCode.SUCCESS, null);
    }

    @ApiModelProperty("错误码 200为成功，其他为失败")
    private int code;

    @ApiModelProperty("错误码中文信息")
    private String message;

    @ApiModelProperty("响应结果")
    private T result;


    /**
     * 返回参数错误
     * @Author wangyi
     * @Date 2020/9/18
     * @param message 消息正文
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getParamError(String message) {
        return ApiResult.getResult(ApiResultCode.PARAMETER_VERIFY_ERROR, message, null);
    }

    /***
     * 幂等错误
     * @Author wangyi
     * @Date 2020/9/21
     * @param message 错误信息
     * @return com.linkkap.webbase.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getIdempotentError(String message) {
        return ApiResult.getResult(ApiResultCode.IDEMPOTENT_ERROR, message, null);
    }

    /**
     * 系统异常
     * @Author wangyi
     * @Date 2020/9/18
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getSystemError() {
        return ApiResult.getResult(ApiResultCode.SYSTEM_ERROR, ApiResultCode.SYSTEM_ERROR.getMessage(), null);
    }


    /**
     * 系统异常
     * @Author wangyi
     * @Date 2020/9/18
     * @return com.linkkap.webbase.response.result.ApiResult<T>
     */
    public static  ApiResult<Throwable> getSystemError(Throwable error) {
        return ApiResult.getResult(ApiResultCode.SYSTEM_ERROR, ApiResultCode.SYSTEM_ERROR.getMessage(), error);
    }

    /**
     * 业务异常
     * @Author wangyi
     * @Date 2020/9/21
     * @return com.linkkap.webbase.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getBusinessError() {
        return ApiResult.getResult(ApiResultCode.UNKNOWN, ApiResultCode.UNKNOWN.getMessage(), null);
    }


    /**
     * 业务异常
     * @Author wangyi
     * @Date 2020/9/21
     * @return com.linkkap.webbase.result.ApiResult<T>
     */
    public static ApiResult<List<String>> getBusinessError(Throwable error) {
        List<String> list = Arrays.stream(error.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.toList());
        return ApiResult.getResult(ApiResultCode.UNKNOWN, ApiResultCode.UNKNOWN.getMessage(), list);
    }

    /**
     * 业务异常
     * @Author wangyi
     * @Date 2020/9/21
     * @return com.linkkap.webbase.result.ApiResult<T>
     */
    public static <T> ApiResult<T> getBusinessError(String message) {
        return ApiResult.getResult(ApiResultCode.UNKNOWN, message, null);
    }

    /***
     * 抛出异常信息
     * @author wangyi
     * @date  2020/11/26
     * @param message 消息
     * @param error 异常信息
     * @return com.linkkap.web.base.result.ApiResult<java.lang.Throwable>
     */
    public static ApiResult<Throwable> getBusinessError(String message,Throwable error) {
        return ApiResult.getResult(ApiResultCode.UNKNOWN, message, error);
    }


    /**
     * 获取数据不存在响应结果
     * @Author wangyi
     * @Date 2020/9/21
     * @param message
     * @return com.linkkap.webbase.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getDataNotError(String message) {
        return ApiResult.getResult(ApiResultCode.DATA_ERROR, message, null);
    }

    /**
     * 返回成功结果 只设置响应信息
     * @Author wangyi
     * @Date 2020/9/18
     * @param message 响应的消息正文
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getSuccess(String message) {
        return ApiResult.getResult(ApiResultCode.SUCCESS, message, null);
    }

    /***
     * 返回成功结果，自定义响应结果
     * @Author wangyi
     * @Date 2020/9/18
     * @param result 响应结果
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getSuccess(T result) {
        return ApiResult.getResult(ApiResultCode.SUCCESS, ApiResultCode.SUCCESS.getMessage(), result);
    }

    /**
     * 返回成功结果，自定义响应结果和响应信息
     * @Author wangyi
     * @Date 2020/9/18
     * @param message 响应消息
     * @param result 响应结果
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getSuccess(String message, T result) {
        return ApiResult.getResult(ApiResultCode.SUCCESS, message, result);
    }

    /**
     * 给定响应码和响应体返回自定义响应结果
     * @Author wangyi
     * @Date 2020/9/21
     * @param code
     * @param result 要写入的结果
     * @return com.linkkap.webbase.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getResult(ApiResultCode code, T result) {
        return ApiResult.getResult(code, code.getMessage(), result);
    }

    /**
     * 响应结果基础方法
     * @Author wangyi
     * @Date 2020/9/18
     * @param code 响应状态码
     * @param message 响应消息
     * @param result 响应结果
     * @return com.linkkap.webbase.response.result.ApiResult<T>
    */
    public static <T> ApiResult<T> getResult(ApiResultCode code, String message, T result) {
        return new ApiResult<T>()
                .setCode(code.getCode())
                .setMessage(message)
                .setResult(result);
    }
}
