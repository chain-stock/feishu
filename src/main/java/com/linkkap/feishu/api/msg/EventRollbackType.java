package com.linkkap.feishu.api.msg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author huang_qiu_sheng
 * */
public class EventRollbackType {
    /**
     * 机器人被邀请加入群聊时触发此事件
     * */
    public static final String ADD_BOT = "add_bot";
    /**
     * 机器人被从群聊中移除时触发此事件
     * */
    public static final String REMOVE_BOT = "remove_bot";
    /**
     *用户和机器人的会话首次被创建
     * */
    public static final String P2P_CHAT_CREATE = "p2p_chat_create";
    /**
     * 当用户发送消息给机器人或在群聊中@机器人时触发此事件
     * */
    public static final String MESSAGE = "message";
    /**
     * 用户阅读机器人发送的消息后触发此事件
     * */
    public static final String MESSAGE_READ = "message_read";

    public static final Set<String> TYPES = new HashSet<String>(Arrays.asList(ADD_BOT, REMOVE_BOT, P2P_CHAT_CREATE, MESSAGE, MESSAGE_READ));
}
