package com.linkkap.feishu.api.msg;

import com.linkkap.feishu.api.msg.richtext.EncodingEnum;
import com.linkkap.feishu.api.msg.richtext.RichText;
import com.linkkap.feishu.api.msg.richtext.RichTextContent;
import com.linkkap.feishu.api.msg.richtext.RichTextLine;

/**
 * 一些默认的
 * @author huang_qiu_sheng
 * @date 3/30/2021 9:25 AM
 */
public class MessageResources {
    public static String handleUserInput(String openId){
        RichText richText = new RichText();

        richText.addOpenId(openId);

        RichTextContent content = new RichTextContent("欢迎使用链股");
        content.addTextLine(new RichTextLine()
                .addText("链股是一个一站式股权激励智能平台应用，在链股机器人中你可以接收：").done());
        content.addTextLine(new RichTextLine().addText("- 期权授予签字").done());
        content.addTextLine(new RichTextLine().addText("- 期权成熟消息").done());
        content.addTextLine(new RichTextLine().addText("- 期权行权签字").done());
        content.addTextLine(new RichTextLine().addText("如果你有股书账号，请在链股SaaS系统-技术支持 反馈，告知需绑定飞书，我们会及时帮你绑定").done());
        content.addTextLine(new RichTextLine().addText("如果你没有链股账号，请点此 ").addUrl("了解更多", "https://www.linkkap.com/about").addText(" 或 ").addUrl("直接注册", "https://web.linkkap.com/register.html").done());
        content.addTextLine(new RichTextLine().addText("更多操作请参见：").addUrl("帮助文档", "https://www.linkkap.com").done());
        content.addTextLine(new RichTextLine().addText("也可以随时反馈：").addUrl("在线反馈", "https://www.linkkap.com").done());
        richText.addContent(EncodingEnum.ZH_CN, content.done());
        return  richText.done().toString();
    }
    public static String loadDefaultMessage(){
        RichText richText = new RichText();

        RichTextContent content = new RichTextContent("欢迎使用链股");
        content.addTextLine(new RichTextLine()
                .addText("链股是一个一站式股权激励智能平台应用，在链股机器人中你可以接收：").done());
        content.addTextLine(new RichTextLine().addText("- 期权授予签字").done());
        content.addTextLine(new RichTextLine().addText("- 期权成熟消息").done());
        content.addTextLine(new RichTextLine().addText("- 期权行权签字").done());
        content.addTextLine(new RichTextLine().addText("如果你有股书账号，请在链股SaaS系统-技术支持 反馈，告知需绑定飞书，我们会及时帮你绑定").done());
        content.addTextLine(new RichTextLine().addText("如果你没有链股账号，请点此 ").addUrl("了解更多", "https://www.linkkap.com/about").addText(" 或 ").addUrl("直接注册", "https://web.linkkap.com/register.html").done());
        content.addTextLine(new RichTextLine().addText("更多操作请参见：").addUrl("帮助文档", "https://www.linkkap.com").done());
        content.addTextLine(new RichTextLine().addText("也可以随时反馈：").addUrl("在线反馈", "https://www.linkkap.com").done());

        richText.addContent(EncodingEnum.ZH_CN, content.done());
        return  richText.done().toString();
    }
}
