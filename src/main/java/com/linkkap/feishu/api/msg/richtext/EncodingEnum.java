package com.linkkap.feishu.api.msg.richtext;

/**
 *
 * 用于指定消息体的语言、需要国际化可以增加ja_jp（日语）、en_us（英文）
 * @author huang_qiu_sheng
 * */

public enum EncodingEnum {
    /**
     * zh_cn
     * */
    ZH_CN("zh_cn"),
    /**
     * ja_jp
     * */
    JA_JP("ja_jp"),
    /**
     * en_us
     * */
    EN_US("en_us");
    private final String encoding;

    EncodingEnum(String encoding) {
        this.encoding = encoding;
    }

    public String getEncoding() {
        return encoding;
    }
}
