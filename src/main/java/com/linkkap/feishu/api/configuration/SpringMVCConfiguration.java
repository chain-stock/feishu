package com.linkkap.feishu.api.configuration;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.Parser;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.linkkap.feishu.api.intercepter.GlobalRequestInterceptor;

import cn.hutool.core.date.LocalDateTimeUtil;

@Configuration
public class SpringMVCConfiguration  implements WebMvcConfigurer {

    /**
     * 日期出参转换
     */
    private static final String DATE_FORMATTER_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        JavaTimeModule module = new JavaTimeModule();
        //序列化规则和反序列化规则
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DATE_FORMATTER_PATTERN)));
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DATE_FORMATTER_PATTERN)));
        objectMapper.registerModule(module);
        return objectMapper;
    }

    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        fastJsonConfig.setDateFormat(DATE_FORMATTER_PATTERN);
        fastConverter.setFastJsonConfig(fastJsonConfig);
        return new HttpMessageConverters(fastConverter);
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER_PATTERN);
        registry.addParser(new Parser<LocalDateTime>() {
            @NonNull
            @Override
            public LocalDateTime parse(@NonNull String text, @NonNull Locale locale) throws ParseException {
                return LocalDateTimeUtil.parse(text, formatter);
            }
        });
    }


//    @Bean
//    public GlobalRequestInterceptor globalRequestInterceptor() {
//        return new GlobalRequestInterceptor();
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(globalRequestInterceptor()).addPathPatterns("/**");
//    }

}