#!/bin/bash
set -m
source /etc/profile

#export M2_HOME=/data/services/apache-maven-3.6.3
#export M2=$M2_HOME/bin
#export MAVEN_OPTS="-Xms256m -Xmx512m"
#export PATH=$M2:$PATH:

project_path=/data/code/feishu

cd $project_path
git pull

rm -rf $project_path/src/main/resources/application.properties
cp -r /data/conf/env/feishu.application.properties  $project_path/src/main/resources/application.properties

mvn clean package -Dmaven.test.skip=true
kill -9 `netstat -nlp | grep 23111 | awk '{print $7}'|awk -F/ '{print $1}'`

mkdir -p /data/logs/feishu/

nohup mvn  exec:java -Dexec.mainClass=com.linkkap.feishu.api.FeishuApplication >  /data/logs/feishu/feishu-api.log 2>&1 &

