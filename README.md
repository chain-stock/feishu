# 运行

可以在Linux里，使用shell/build.sh运行

## 接口介绍

本服务提供了两个接口，一个负责提供对应的事件和富文本信息（默认覆盖），一个负责删除对应的事件。

测试事件的代码在：FeiShuRobotControllerExampleUsage.testEvent使用时记得使用open_id，否则会发送到其他人。

测试修改事件和删除事件：UpdateEventTypeTest中的方法

### 提供对应的事件和富文本信息（如果有默认覆盖）
访问地址为：/feishu/eventRollbackType.do

使用POST方法提供如下JSON数据示例
```json
{
  "event_type": "message",
  "content": {
    "msg_type": "post",
    "content": {
      "post": {
        "zh_cn": {
          "title": "欢迎使用链股",
          "content": [
            [
              {
                "tag": "text",
                "text": "链股是一个一站式股权激励智能平台应用，在链股机器人中你可以接收："
              }
            ],
            [
              {
                "tag": "text",
                "text": "- 期权授予签字"
              }
            ],
            [
              {
                "tag": "text",
                "text": "- 期权成熟消息"
              }
            ],
            [
              {
                "tag": "text",
                "text": "- 期权行权签字"
              }
            ],
            [
              {
                "tag": "text",
                "text": "如果你有股书账号，请在链股SaaS系统-技术支持 反馈，告知需绑定飞书，我们会及时帮你绑定"
              }
            ],
            [
              {
                "tag": "text",
                "text": "如果你没有链股账号，请点此 "
              },
              {
                "tag": "a",
                "text": "了解更多",
                "href": "https://www.linkkap.com/about"
              },
              {
                "tag": "text",
                "text": " 或 "
              },
              {
                "tag": "a",
                "text": "直接注册",
                "href": "https://web.linkkap.com/register.html"
              }
            ],
            [
              {
                "tag": "text",
                "text": "更多操作请参见："
              },
              {
                "tag": "a",
                "text": "帮助文档",
                "href": "https://www.linkkap.com"
              }
            ],
            [
              {
                "tag": "text",
                "text": "也可以随时反馈："
              },
              {
                "tag": "a",
                "text": "在线反馈",
                "href": "https://www.linkkap.com"
              }
            ]
          ]
        }
      }
    }
  }
}
```
也可以使用如下方法构建
```java
RichText richText = new RichText();

RichTextContent content = new RichTextContent("欢迎使用链股");
content.addTextLine(new RichTextLine()
        .addText("链股是一个一站式股权激励智能平台应用，在链股机器人中你可以接收：").done());
content.addTextLine(new RichTextLine().addText("- 期权授予签字").done());
content.addTextLine(new RichTextLine().addText("- 期权成熟消息").done());
content.addTextLine(new RichTextLine().addText("- 期权行权签字").done());
content.addTextLine(new RichTextLine().addText("如果你有股书账号，请在链股SaaS系统-技术支持 反馈，告知需绑定飞书，我们会及时帮你绑定").done());
content.addTextLine(new RichTextLine().addText("如果你没有链股账号，请点此 ").addUrl("了解更多", "https://www.linkkap.com/about").addText(" 或 ").addUrl("直接注册", "https://web.linkkap.com/register.html").done());
content.addTextLine(new RichTextLine().addText("更多操作请参见：").addUrl("帮助文档", "https://www.linkkap.com").done());
content.addTextLine(new RichTextLine().addText("也可以随时反馈：").addUrl("在线反馈", "https://www.linkkap.com").done());

richText.addContent(EncodingEnum.ZH_CN, content.done());

json.put("content", richText.done());
```
### 删除一个事件
访问地址为：/feishu/eventRollbackType.do

使用DELETE方法提供如下JSON数据示例

```json
{
  "event_type": "message"
}
```

